import { render, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'

import { UiButton } from './index'

describe('<UiButton />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<UiButton>Pradeep</UiButton>)
    expect(getByText(/Pradeep/i)).toBeInTheDocument()
  })

  test('should invoke onclick handler correctly, while clicking on button', () => {
    const handleClick = jest.fn()
    const { getByText } = render(
      <UiButton onClickHandler={handleClick}>Pradeep</UiButton>
    )
    const button = getByText('Pradeep')
    fireEvent.click(button)
    expect(handleClick).toHaveBeenCalled()
  })

  test('should not invoke onclick handler, when its not passed as prop', () => {
    const handleClick = jest.fn()
    const { getByText } = render(<UiButton>Pradeep</UiButton>)
    const button = getByText('Pradeep')
    fireEvent.click(button)
    expect(handleClick).not.toHaveBeenCalled()
  })
})
